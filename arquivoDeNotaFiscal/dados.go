package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Dados struct {
	TipoRegistro          int32 	`json:"TipoRegistro"`
	DataSaidaMercadoria   int32 	`json:"DataSaidaMercadoria"`
	HoraSaidaMercadoria   int32 	`json:"HoraSaidaMercadoria"`
	DataEmissaoNotaFiscal int32 	`json:"DataEmissaoNotaFiscal"`
	CnpjLoja              string	`json:"CnpjLoja"`
	NumeroNotaFiscal      int32 	`json:"NumeroNotaFiscal"`
	SerieDOcumento        string	`json:"SerieDOcumento"`
	VencimentoNotaFiscal  int32 	`json:"VencimentoNotaFiscal"`
	Livre                 string	`json:"Livre"`
}

func (d *Dados) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataSaidaMercadoria, "DataSaidaMercadoria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.HoraSaidaMercadoria, "HoraSaidaMercadoria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.DataEmissaoNotaFiscal, "DataEmissaoNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CnpjLoja, "CnpjLoja")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroNotaFiscal, "NumeroNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SerieDOcumento, "SerieDOcumento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.VencimentoNotaFiscal, "VencimentoNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Livre, "Livre")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDados = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"DataSaidaMercadoria":                      {1, 9, 0},
	"HoraSaidaMercadoria":                      {9, 15, 0},
	"DataEmissaoNotaFiscal":                      {15, 23, 0},
	"CnpjLoja":                      {23, 37, 0},
	"NumeroNotaFiscal":                      {37, 43, 0},
	"SerieDOcumento":                      {43, 46, 0},
	"VencimentoNotaFiscal":                      {46, 54, 0},
	"Livre":                      {54, 80, 0},
}