package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Dados2 struct {
	TipoRegistro                      int32  	`json:"TipoRegistro"`
	BaseCalculoICMS                   float32	`json:"BaseCalculoICMS"`
	ValorICMS                         float32	`json:"ValorICMS"`
	BaseCalculoSubstituicaoTributaria float32	`json:"BaseCalculoSubstituicaoTributaria"`
	ValorICMSSubstituicaoTributaria   float32	`json:"ValorICMSSubstituicaoTributaria"`
	Livre                             string 	`json:"Livre"`
}

func (d *Dados2) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados2

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorICMS, "ValorICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.BaseCalculoSubstituicaoTributaria, "BaseCalculoSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorICMSSubstituicaoTributaria, "ValorICMSSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Livre, "Livre")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDados2 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"BaseCalculoICMS":                      {1, 9, 2},
	"ValorICMS":                      {9, 17, 2},
	"BaseCalculoSubstituicaoTributaria":                      {17, 25, 2},
	"ValorICMSSubstituicaoTributaria":                      {25, 33, 2},
	"Livre":                      {33, 80, 0},
}