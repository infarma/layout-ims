package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	TipoRegistro           int32  	`json:"TipoRegistro"`
	CodigoEan              string 	`json:"CodigoEan"`
	CodigoProdutoOperador  string 	`json:"CodigoProdutoOperador"`
	Quantidade             int32  	`json:"Quantidade"`
	Unidade                string 	`json:"Unidade"`
	PrecoFabrica           float32	`json:"PrecoFabrica"`
	DescontoComercial      float32	`json:"DescontoComercial"`
	ValorDescontoComercial float32	`json:"ValorDescontoComercial"`
	ValorRepasse           float32	`json:"ValorRepasse"`
	Repasse                float32	`json:"Repasse"`
	ValorUnitario          float32	`json:"ValorUnitario"`
	Fracionamento          float32	`json:"Fracionamento"`
	Livre                  string 	`json:"Livre"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoEan, "CodigoEan")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProdutoOperador, "CodigoProdutoOperador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Unidade, "Unidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoFabrica, "PrecoFabrica")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescontoComercial, "DescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorDescontoComercial, "ValorDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorRepasse, "ValorRepasse")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Repasse, "Repasse")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitario, "ValorUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Fracionamento, "Fracionamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Livre, "Livre")
	if err != nil {
		return err
	}


	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CodigoEan":                      {1, 14, 0},
	"CodigoProdutoOperador":                      {14, 21, 0},
	"Quantidade":                      {21, 25, 0},
	"Unidade":                      {25, 28, 0},
	"PrecoFabrica":                      {28, 36, 2},
	"DescontoComercial":                      {36, 40, 2},
	"ValorDescontoComercial":                      {40, 48, 2},
	"ValorRepasse":                      {48, 56, 2},
	"Repasse":                      {56, 60, 2},
	"ValorUnitario":                      {60, 68, 2},
	"Fracionamento":                      {68, 72, 2},
	"Livre":                      {72, 80, 0},
}