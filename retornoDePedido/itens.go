package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	TipoRegistro          int32 	`json:"TipoRegistro"`
	EanProduto            int64 	`json:"EanProduto"`
	QuantidadeAtendida    int32 	`json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int32 	`json:"QuantidadeNaoAtendida"`
	SemUtilizacao         string	`json:"SemUtilizacao"`
	Retorno               int32 	`json:"Retorno"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.EanProduto, "EanProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeNaoAtendida, "QuantidadeNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Retorno, "Retorno")
	if err != nil {
		return err
	}


	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"EanProduto":                      {1, 14, 0},
	"QuantidadeAtendida":                      {14, 18, 0},
	"QuantidadeNaoAtendida":                      {18, 22, 0},
	"SemUtilizacao":                      {22, 24, 0},
	"Retorno":                      {24, 25, 0},
}