package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoRegistro                   int32 	`json:"TipoRegistro"`
	QuantidadeUnidadesAtendidas    int32 	`json:"QuantidadeUnidadesAtendidas"`
	QuantidadeUnidadesNaoAtendidas int32 	`json:"QuantidadeUnidadesNaoAtendidas"`
	QuantidadeItensArquivo         int32 	`json:"QuantidadeItensArquivo"`
	SemUtilizacao                  string	`json:"SemUtilizacao"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeUnidadesAtendidas, "QuantidadeUnidadesAtendidas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeUnidadesNaoAtendidas, "QuantidadeUnidadesNaoAtendidas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeItensArquivo, "QuantidadeItensArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}


	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"QuantidadeUnidadesAtendidas":                      {1, 5, 0},
	"QuantidadeUnidadesNaoAtendidas":                      {5, 9, 0},
	"QuantidadeItensArquivo":                      {9, 13, 0},
	"SemUtilizacao":                      {13, 25, 0},
}