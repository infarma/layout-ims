package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro          int32 	`json:"TipoRegistro"`
	CnpjCliente           string	`json:"CnpjCliente"`
	CodigoProjeto         string	`json:"CodigoProjeto"`
	PedidoIMSDemandaFarma string	`json:"PedidoIMSDemandaFarma"`
	SemUtilizacao         string	`json:"SemUtilizacao"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoProjeto, "CodigoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PedidoIMSDemandaFarma, "PedidoIMSDemandaFarma")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}


	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CnpjCliente":                      {1, 15, 0},
	"CodigoProjeto":                      {15, 16, 0},
	"PedidoIMSDemandaFarma":                      {16, 23, 0},
	"SemUtilizacao":                      {23, 25, 0},
}