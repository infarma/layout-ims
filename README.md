## Arquivo de Pedido
gerador-layouts arquivoDePedido cabecalho TipoRegistro:int32:0:1 CnpjCliente:string:1:15 TipoFaturamento:int32:15:16 ApontadorPromocao:string:16:29 CodigoControle:string:29:31

gerador-layouts arquivoDePedido dados1 TipoRegistro:int32:0:1 CodigoProjeto:string:1:2 PedidoIMSDemandaFarma:string:2:9 CnpjCentroDistribuicao:string:9:23 SemUtilizacao:string:23:31

gerador-layouts arquivoDePedido faturamento TipoRegistro:int32:0:1 TipoPagamento:int32:1:2 CodigoPrazoDeterminado:string:2:6 NumeroDiasPrazoDeterminado:int32:6:9 NumeroPedidoPrincipal:int32:9:16 SemUtilizacao:string:16:31

gerador-layouts arquivoDePedido dados2 TipoRegistro:int32:0:1 NumeroPedidoCliente:string:1:16 SemUtilizacoa:string:16:31

gerador-layouts arquivoDePedido data TipoRegistro:int32:0:1 Data:int32:1:9 SemUtilizacao:string:9:31

gerador-layouts arquivoDePedido hora TipoRegistro:int32:0:1 Hora:int32:1:7 SemUtilizacao:string:7:31

gerador-layouts arquivoDePedido itens TipoRegistro:int32:0:1 CodigoEanProduto:int64:1:14 Quantidade:int32:14:18 TipoOcorrencia:int32:18:20 CampoControleIMS:string:20:27 DescontoItem:float32:27:31:2

gerador-layouts arquivoDePedido rodape TipoRegistro:int32:0:1 QuantidadeItens:int32:1:3 SemUtilizacao:string:3:31


## Arquivo de Retorno
gerador-layouts retornoDePedido cabecalho TipoRegistro:int32:0:1 CnpjCliente:string:1:15 CodigoProjeto:string:15:16 PedidoIMSDemandaFarma:string:16:23 SemUtilizacao:string:23:25

gerador-layouts retornoDePedido dados TipoRegistro:int32:0:1 Data:int32:1:9 Hora:int32:9:15 SemUtilizacao:string:15:25

gerador-layouts retornoDePedido itens TipoRegistro:int32:0:1 EanProduto:int64:1:14 QuantidadeAtendida:int32:14:18 QuantidadeNaoAtendida:int32:18:22 SemUtilizacao:string:22:24 Retorno:int32:24:25

gerador-layouts retornoDePedido rodape TipoRegistro:int32:0:1 QuantidadeUnidadesAtendidas:int32:1:5 QuantidadeUnidadesNaoAtendidas:int32:5:9 QuantidadeItensArquivo:int32:9:13 SemUtilizacao:string:13:25


## Arquivo de Nota Fiscal
gerador-layouts arquivoDeNotaFiscal cabecalho TipoRegistro:int32:0:1 DataGeracaoArquivo:int32:1:9 HoraGeracaoArquivo:int32:9:15 CnpjOperador:int64:15:29 CodigoProjeto:string:29:30 NumeroPedidoIMSDemandaFarma:int32:30:37 NumeroPedidoCliente:string:37:52 Livre:string:52:80

gerador-layouts arquivoDeNotaFiscal dados1 TipoRegistro:int32:0:1 DataSaidaMercadoria:int32:1:9 HoraSaidaMercadoria:int32:9:15 DataEmissaoNotaFiscal:int32:15:23 CnpjLoja:string:23:37 NumeroNotaFiscal:int32:37:43 SerieDOcumento:string:43:46 VencimentoNotaFiscal:int32:46:54 Livre:string:54:80

gerador-layouts arquivoDeNotaFiscal totais TipoRegistro:int32:0:1 ValorFrete:float32:1:9:2 ValorSeguro:float32:9:17:2 OutrasDespesas:float32:17:25:2 ValorTotalProdutos:float32:25:33:2 ValorTotalNF:float32:33:41:2 ValorIPI:float32:41:49:2 Livre:string:49:80

gerador-layouts arquivoDeNotaFiscal dados2 TipoRegistro:int32:0:1 BaseCalculoICMS:float32:1:9:2 ValorICMS:float32:9:17:2 BaseCalculoSubstituicaoTributaria:float32:17:25:2 ValorICMSSubstituicaoTributaria:float32:25:33:2 Livre:string:33:80

gerador-layouts arquivoDeNotaFiscal itens TipoRegistro:int32:0:1 CodigoEan:string:1:14 CodigoProdutoOperador:string:14:21 Quantidade:int32:21:25 Unidade:string:25:28 PrecoFabrica:float32:28:36:2 DescontoComercial:float32:36:40:2 ValorDescontoComercial:float32:40:48:2 ValorRepasse:float32:48:56:2 Repasse:float32:56:60:2 ValorUnitario:float32:60:68:2 Fracionamento:float32:68:72:2 Livre:string:72:80

gerador-layouts arquivoDeNotaFiscal rodape TipoRegistro:int32:0:1 NumeroItensNotaFiscal:int32:1:5 Livre:5:80