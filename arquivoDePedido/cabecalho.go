package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro      int32 	`json:"TipoRegistro"`
	CnpjCliente       string	`json:"CnpjCliente"`
	TipoFaturamento   int32 	`json:"TipoFaturamento"`
	ApontadorPromocao string	`json:"ApontadorPromocao"`
	CodigoControle    string	`json:"CodigoControle"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoFaturamento, "TipoFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ApontadorPromocao, "ApontadorPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoControle, "CodigoControle")
	if err != nil {
		return err
	}


	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CnpjCliente":                      {1, 15, 0},
	"TipoFaturamento":                      {15, 16, 0},
	"ApontadorPromocao":                      {16, 29, 0},
	"CodigoControle":                      {29, 31, 0},
}