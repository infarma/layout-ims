package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho   Cabecalho   `json:"Cabecalho"`
	Dados1      Dados1      `json:"Dados1"`
	Faturamento Faturamento `json:"Faturamento"`
	Dados2      Dados2      `json:"Dados2"`
	Data        Data        `json:"Data"`
	Hora        Hora        `json:"Hora"`
	Itens       []Itens     `json:"Itens"`
	Rodape      Rodape      `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		var index int32
		if identificador == "1" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.Dados1.ComposeStruct(string(runes))
		} else if identificador == "3" {
			err = arquivo.Faturamento.ComposeStruct(string(runes))
		} else if identificador == "4" {
			err = arquivo.Dados2.ComposeStruct(string(runes))
		} else if identificador == "5" {
			err = arquivo.Data.ComposeStruct(string(runes))
		} else if identificador == "6" {
			err = arquivo.Hora.ComposeStruct(string(runes))
		} else if identificador == "7" {
			err = arquivo.Itens[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "8" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
