package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Dados2 struct {
	TipoRegistro        int32 	`json:"TipoRegistro"`
	NumeroPedidoCliente string	`json:"NumeroPedidoCliente"`
	SemUtilizacoa       string	`json:"SemUtilizacoa"`
}

func (d *Dados2) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados2

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacoa, "SemUtilizacoa")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDados2 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"NumeroPedidoCliente":                      {1, 16, 0},
	"SemUtilizacoa":                      {16, 31, 0},
}