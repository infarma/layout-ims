package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Dados struct {
	TipoRegistro  int32 	`json:"TipoRegistro"`
	Data          int32 	`json:"Data"`
	Hora          int32 	`json:"Hora"`
	SemUtilizacao string	`json:"SemUtilizacao"`
}

func (d *Dados) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Data, "Data")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Hora, "Hora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDados = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"Data":                      {1, 9, 0},
	"Hora":                      {9, 15, 0},
	"SemUtilizacao":                      {15, 25, 0},
}