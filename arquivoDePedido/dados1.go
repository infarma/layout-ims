package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Dados1 struct {
	TipoRegistro           int32 	`json:"TipoRegistro"`
	CodigoProjeto          string	`json:"CodigoProjeto"`
	PedidoIMSDemandaFarma  string	`json:"PedidoIMSDemandaFarma"`
	CnpjCentroDistribuicao string	`json:"CnpjCentroDistribuicao"`
	SemUtilizacao          string	`json:"SemUtilizacao"`
}

func (d *Dados1) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDados1

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoProjeto, "CodigoProjeto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PedidoIMSDemandaFarma, "PedidoIMSDemandaFarma")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CnpjCentroDistribuicao, "CnpjCentroDistribuicao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.SemUtilizacao, "SemUtilizacao")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDados1 = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CodigoProjeto":                      {1, 2, 0},
	"PedidoIMSDemandaFarma":                      {2, 9, 0},
	"CnpjCentroDistribuicao":                      {9, 23, 0},
	"SemUtilizacao":                      {23, 31, 0},
}